Google Cloud Example
====================

This repository contains code for setting up a cloud service using `Google Cloud`_ and `Kubernetes`_ in order to run multiple jobs in paralell.
The main part of the code is based on the code in the repository: https://github.com/willcrichton/gcp-job-queue which also comes with a very nice descriptioon of the setup here:  http://willcrichton.net/notes/gcp-job-queue/



.. _`Google Cloud`: https://cloud.google.com`
.. _`Kubernetes`: https://kubernetes.io



Before you start
----------------

Before you start you need to do the following

1. Setting up a Python Development Environment, see `<https://cloud.google.com/python/setup>`_ for a good tutorial on how to do this. 

2. Install `Docker <https://docs.docker.com/install/>`_
	  
3.  `Kubernetes <https://kubernetes.io>`_ command line interface

     ::


	gcloud components install kubectl


Setting up gcloud
-----------------

Now if you prefer you can use your web-browser and go to `<https://console.cloud.google.com>`_. This will show you all the projects, containers, clusters, etc.

From the command line get the project name

::

   gcloud config get-value project
