Google cloud
============

   
   
Starting Google Cloud
---------------------

First you need to set up a Google Cloud Platform account and install the command line interact ``glcoud``.
Have a look at `Running Kubernetes on Google Compute Engine`_.
Also For installation have a look at `quickstarts <https://cloud.google.com/sdk/docs/quickstarts>`_
and the `reference guide <https://cloud.google.com/sdk/gcloud/reference/>`_
Finally, there is a `How to <https://cloud.google.com/kubernetes-engine/docs/how-to/>`_ section which is also useful.

.. _`Running Kubernetes on Google Compute Engine`: https://kubernetes.io/docs/getting-started-guides/gce/


Commands
--------


List the accounts

::

   gcloud auth list
   

Get some info

::

   gcloud info

Get config info

::

   gcloud config list


List your project

::

   gcloud projects list

List your instances

::

   gcloud compute instances list


Create an instance ``test-instances``

::

   gcloud compute instances create test-instance

Delete an instance ``test-instances``

::

   gcloud compute instances delete test-instance


Create a container cluster ``test-container-cluster``

::

   gcloud container clusters create test-container-cluster


Get info about container cluster

::

   gcloud container clusters describe test-container-cluster


Set default cluster

::

   gcloud config set container/cluster test-container-cluster

Delete a container cluster ``test-container-cluster``

::

   gcloud container clusters delete test-container-cluster
   
List available clusters

::

   gcloud container clusters list


Get certificate from a given cluster. None that this will also configure ``kubectl``.

::

   gcloud container clusters get-credentials <cluster name>

