Cloud Storage
=============


In this example we will create a cloud storage bucket and ..
For more information about buckets, see `<https://cloud.google.com/python/getting-started/using-cloud-storage>`_.
If you want to learn more about the ``gsutil`` command, have a look at `<https://cloud.google.com/storage/docs/quickstart-gsutil>`_


Creating the bucket
-------------------

The first thing we need to do is to create a bucket. Lets call it ``test-bucket-x``, where ``x`` is the number in your project.

Create bucket 

::

   gsutil mb gs://test-bucket-x



.. note::

   If you get the followin error ``ServiceException: 409 Bucket test-bucket already exists.``, then someone else have created a bucket with that name. Try with a different name.

Have a look inside your bucket

::

   gsutil ls gs://test-bucket-x

Now try to create a file and copy it to the bucket

::

   echo "Hello bucket" > hello.txt
   gsutil cp hello.txt gs://test-bucket-x
   gsutil ls gs://test-bucket-x


You will see that the file ``hello.txt`` are now in your bucket.
Now try to remove that file locally, and copy it back from the bucket.

::

   rm hello.txt
   ls
   <see that the file is gone>
   gsutil cp gs://test-bucket-x/hello.txt .
   ls
   <see that the file is back again>

