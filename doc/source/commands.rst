Kubernetes
==========

Definitions
-----------

Notes
    Hosts that run the Kuberbernetes application.

`Pod`_
    The smallest deployable object in the Kubernetes object model.
    Represents a running process on your cluster. Can be one or a group of containers.
    Similar to a task on a supercomputer. Unit of deployment.
    Collection of homogenous containers.
    
`Job`_
    A job creates one or more pods and ensured that they successfully terminate.
    Jobs are specific in a ``*.yaml`` file. If pod fails, the job will automatically
    start a new pod, and it will also clean up afterwards. 
    
`Container`_
    An container is a lightweight version of a virtual machince (VM).
    In contrast to VMs container does not have its own operating system.
    `Docker`_ is an example of a software technology for hanlding containers.

`Repplication Controller`_
    Ensures availablity and scalability.

`Replica Set`_
    Similar to replication controller but which is desinged to support the new set-based selector.
    If a pod fails it will be rescheduled.

`Deamon Set`_
    Similar to Replica set, but have different use cases.
    It runs a copy of a Pod. Runs one pod per node (this is not the case for replica sets).
    Therefore the number of pods is equal to the number of nodes.
    If a pod / node fails it will not be reschedueled.

`Deployment`_


Labels
    Key-Value pairs for identification. 
    
Servies
    Collection of pods exposed as an endpoint
    
.. _`Pod`: https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/
.. _`Job`: https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/
.. _`Container`: https://www.docker.com/what-container
.. _`Docker`: https://www.docker.com
.. _`Repplication Controller`: https://kubernetes.io/docs/concepts/workloads/controllers/replicationcontroller/
.. _`Replica Set`: https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/
.. _`Deamon Set`: https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/
.. _`Deployment`: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/


Cheat sheets
------------

* `kubectl`_


.. _`kubectl`: https://kubernetes.io/docs/reference/kubectl/cheatsheet/


Minikube
--------
`Minikube`_ is a tool that you can us to run Kubernetes locally. This is handy when you want to develop your application withour having to run your software in a cloud. When you are ready to bring your software to other users you should look at other `kubernetes solutions`_.

.. _`kubernetes solutions`: https://kubernetes.io/docs/setup/pick-right-solution/
.. _`Minikube`: https://kubernetes.io/docs/getting-started-guides/minikube/

`Hello Minikube`_ is i good starting point for setting up Minikube and running a simple cluster

.. _`Hello Minikube`: https://kubernetes.io/docs/tutorials/stateless-application/hello-minikube/

Starting minikube
.................

Starting minikube:

::
   
   minikube start --vm-driver=hyperkit


If Minikube fails to start try the following (Mac OSX)

::
   
   rm -rf ~/.minikube
   sudo chown root:wheel $(brew --prefix)/opt/docker-machine-driver-xhyve/bin/docker-machine-driver-xhyve
   sudo chmod u+s $(brew --prefix)/opt/docker-machine-driver-xhyve/bin/docker-machine-driver-xhyve


Make sure that Kubernetes interacts with Minikube (and not any other cluster like e.g Google cloud)

::

   kubectl config use-context "minikube"


Check that Kubernetes interatcs with minikube

::

   kubectl cluster-info

You can also look at you Minikube cluster

::

   minikube dashboard


Set up docker for minikube
..........................

Turn on

::

   eval $(minikube docker-env)


Turn off

::

   eval $(minikube docker-env -u)



Create a deployment
-------------------

A typical scenario is that you have a docker image that you want to run in the cluster.
Suppose you have a Dockerfile it the current working directory

Create docker image

::
   
   docker build -t hello-world:v1

Now we create a deployment called `hello-world` from the image you just created

::

   kubectl run hello-world --image=hello-world:v1

Look at the help for more information

::

   kubectl run --help

.. _`deployment`: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/   

Create a job
------------


   

Get info about your cluster
---------------------------

Get info about your cluster

::
   
  kubectl cluster-info


View the deployments

::

   kubectl get deployments

View the Pods

::

   kubectl get pods


View th cluster events

::

   kubectl get events


View the status of the components

::

   kubectl get cs

View the status of the replication controllers

::

   kubectl get rc


View the status of the services

::

   kubectl get svc

Get info about a particular pod

::

    kubectl describe pod <pod name>


Get info about a particular node

::

    kubectl describe node <node name>

Get info about a particular service

::

    kubectl describe svc <service name>



    
Delete stuff
------------

Delete a pod

::

   kubectl delete pod <pod name>


Delete a service

::

   kubectl delete svc <service name>
   

   
   
Starting Google Cloud
---------------------

First you need to set up a Google Cloud Platform account and install the command line interact ``glcoud``.
Have a look at `Running Kubernetes on Google Compute Engine`_.
Also For installation have a look at `quickstarts <https://cloud.google.com/sdk/docs/quickstarts>`_
and the `reference guide <https://cloud.google.com/sdk/gcloud/reference/>`_
Finally, there is a `How to <https://cloud.google.com/kubernetes-engine/docs/how-to/>`_ section which is also useful.
.. _`Running Kubernetes on Google Compute Engine`: https://kubernetes.io/docs/getting-started-guides/gce/


Commands
........


List the accounts

::

   gcloud auth list
   

Get some info

::

   gcloud info

Get config info

::

   gcloud config list


List your project

::

   gcloud projects list

List your instances

::

   gcloud compute instances list


Create an instance ``test-instances``

::

   gcloud compute instances create test-instance

Delete an instance ``test-instances``

::

   gcloud compute instances delete test-instance


Create a container cluster ``test-container-cluster``

::

   gcloud container clusters create test-container-cluster


Get info about container cluster

::

   gcloud container clusters describe test-container-cluster


Set default cluster

::

   gcloud config set container/cluster test-container-cluster

Delete a container cluster ``test-container-cluster``

::

   gcloud container clusters delete test-container-cluster
   
List available clusters

::

   gcloud container clusters list


Get certificate from a given cluster. None that this will also configure ``kubectl``.

::

   gcloud container clusters get-credentials <cluster name>


Docker
------


Login to Docker

::

   docker login

Next create a `Dockerfile`_, and build your docker image

::

   docker build -t myimage .

where you can change the name ``myimage`` to anything you like.

.. _`Dockerfile`: https://docs.docker.com/engine/reference/builder/

In order to access this image from the cloud you need to push it to some container registry.
The default one is the `Docker Hub`_. If you want to have private repositories you can have 1 for free, and after that you need to pay.
There is also an alternative to move to other container registry, for example `quay.io`_ or the google container registry(`gcr.io`_).

.. _`Docker Hub`: https://hub.docker.com
.. _`quay.io`: https://quay.io
.. _`gcr.io`: https://gcr.io

Assume you want to use Docker hub then you do

::

   docker tag myimage <username>/myimage
   docker push <username>/myimage

where ``<username>`` is you Docker username. If you want to use ``gcr`` then you do

::

   docker tag myimage gcr.io/<project>/myimage
   gcloud docker -- push gcr.io/<project>/myimage


Containers
..........

List current containers

::

   docker ps

List all containers (also the ones that are not currently running)

::

   docker ps -a

   
Stop a running container

::

   docker stop <container id>

Delete a container

::

   docker rm <container id>


Delete all containrs

::

   # Try to stop all running containers
   docker stop $(docker ps -q) #q returns the id
   # If there still exist running containers you can kill the rest
   docker kill $(docker ps -q)
   # Delete all containers
   docker rm $(docker ps -a -q)
   
To start it again do

::

   docker start <container id>


Upgrade container. If you made some chages to a script that runs inside the container,
you can upgrade the container 

::

   docker pull <image tag>
   docker stop <container id>
   # docker kill <container id>
   docker rm <container id>
   # Your run command i.e
   docker build --no-cache . t <image tag>


Images
.......

List current containers

::

   docker images

List all images (also the ones that are not currently running)

::

   docker images -a

   

Delete an image

::

   docker rmi <image id>


If you want to delete all images you can try to do

::

   docker rmi $(docker images -a -q)

but this might fail if you have some images that have a partent
image in the list of images. These images have the same id, and they will show
up twice if you do ``docker images -a q``. But they will have a different name,
so you can instead delete them by name (i.e ``REPOSITORY:TAG``.

::

   docker rmi $(docker images --format '{{.Repository}}:{{.Tag}}')

Redis
-----

`Redis`_ is used to e.g handling queueng system

..

You can install it via ``pip``:

::

   pip install redis

but it turns out that you also need redis-server

On Ubuntu you can do

::

   sudo apt-get install redis-server

On Mac OSX is is available view Homebrew

::

   brew install redis

Start redis

::

   redis-server /usr/local/etc/redis.conf
