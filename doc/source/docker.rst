Docker
======


Login to Docker

::

   docker login

Next create a `Dockerfile`_, and build your docker image

::

   docker build -t myimage .

where you can change the name ``myimage`` to anything you like.

.. _`Dockerfile`: https://docs.docker.com/engine/reference/builder/

In order to access this image from the cloud you need to push it to some container registry.
The default one is the `Docker Hub`_. If you want to have private repositories you can have 1 for free, and after that you need to pay.
There is also an alternative to move to other container registry, for example `quay.io`_ or the google container registry(`gcr.io`_).

.. _`Docker Hub`: https://hub.docker.com
.. _`quay.io`: https://quay.io
.. _`gcr.io`: https://gcr.io

Assume you want to use Docker hub then you do

::

   docker tag myimage <username>/myimage
   docker push <username>/myimage

where ``<username>`` is you Docker username. If you want to use ``gcr`` then you do

::

   docker tag myimage gcr.io/<project>/myimage
   gcloud docker -- push gcr.io/<project>/myimage


Containers
----------

List current containers

::

   docker ps

List all containers (also the ones that are not currently running)

::

   docker ps -a

   
Stop a running container

::

   docker stop <container id>

Delete a container

::

   docker rm <container id>


Delete all containrs

::

   # Try to stop all running containers
   docker stop $(docker ps -q) #q returns the id
   # If there still exist running containers you can kill the rest
   docker kill $(docker ps -q)
   # Delete all containers
   docker rm $(docker ps -a -q)
   
To start it again do

::

   docker start <container id>


Upgrade container. If you made some chages to a script that runs inside the container,
you can upgrade the container 

::

   docker pull <image tag>
   docker stop <container id>
   # docker kill <container id>
   docker rm <container id>
   # Your run command i.e
   docker build --no-cache . t <image tag>


Images
------

List current containers

::

   docker images

List all images (also the ones that are not currently running)

::

   docker images -a

   

Delete an image

::

   docker rmi <image id>


If you want to delete all images you can try to do

::

   docker rmi $(docker images -a -q)

but this might fail if you have some images that have a partent
image in the list of images. These images have the same id, and they will show
up twice if you do ``docker images -a q``. But they will have a different name,
so you can instead delete them by name (i.e ``REPOSITORY:TAG``.

::

   docker rmi $(docker images --format '{{.Repository}}:{{.Tag}}')

