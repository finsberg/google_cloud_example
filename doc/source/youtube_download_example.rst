Downloading Youtube videos
========================== 



1. Install the requirements

   .. code-block:: bash

      $ pip install -r requirements.txt

2. Check that local script is working

   .. code-block:: bash

      $ python local.py
