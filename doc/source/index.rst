.. Google Cloud Example documentation master file, created by
   sphinx-quickstart on Tue Feb 27 11:17:07 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Google Cloud Example's documentation!
================================================

.. toctree::
   :maxdepth: 1
   :caption: Installation

   about

.. toctree::
   :maxdepth: 1
   :caption: Tools:


   docker
   google_cloud
   kubernetes

.. toctree::
   :maxdepth: 1
   :caption: Demos:


   message_queue
   cloud_storage
   youtube_download_example

   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
