Messaging Queue
===============


There exists a variety of tools for setting up a messaging queue. The most known are `Redis`_, `RabbitMQ`_ as well as Google own messaging service `Pub/Sub`_.




.. _`Redis`: https://redis.io
.. _`RabbitMQ`: https://www.rabbitmq.com
.. _`Pub/Sub`: https://cloud.google.com/pubsub/docs/


Google Pub/Sub
--------------


Redis
-----


You can install it via ``pip``:

::

   pip install redis

but it turns out that you also need redis-server

On Ubuntu you can do

::

   sudo apt-get install redis-server

On Mac OSX is is available view Homebrew

::

   brew install redis

Start redis

::

   redis-server /usr/local/etc/redis.conf
