Google Clod Example
===================

This repository contains code for setting up a cloud service using `Google Cloud`_ and `Kubernetes`_ in order to run multiple jobs in paralell.
The main part of the code is based on the code in the repository: https://github.com/willcrichton/gcp-job-queue which also comes with a very nice descriptioon of the setup here:  http://willcrichton.net/notes/gcp-job-queue/



.. _`Google Cloud`: https://cloud.google.com`
.. _`Kubernete`: https://kubernetes.io
