import os
import google.cloud.storage
import config

client = google.cloud.storage.Client(project=config.PROJECT_ID)



def list_buckets():
    """
    List all buckets within the project
    """
    buckets=[]
    for bucket in client.list_buckets():
        buckets.append(bucket.name)

    return buckets



def delete_bucket(bucket_name, force=False):
    """
    Delete the given bucket

    Arguments
    ---------
    bucket_name : str
        Name of the bucket to be deleted
    force : bool
        Force deletion

    """
    if bucket_name in list_buckets():
        bucket = client.get_bucket(bucket_name)
        bucket.delete(force=force)

        msg = "Bucket {} deleted".format(bucket_name)
    else:
        msg = "Bucket {} does not exists".format(bucket_name)

    print(msg)
        

    
class Bucket(object):
    """
    Wrapping of google.cloud.storage.Bucket

    Arguments
    ---------
    bucket_name : str
        Name of the bucket


    """
    
    def __init__(self, bucket_name):

        
        buckets = list_buckets()
        
        # Check if bucket allready exists
        if bucket_name in buckets:
            msg = "Load bucket {}".format(bucket_name)
            print(msg)
            self._bucket = client.get_bucket(bucket_name)

        else:
            # Create bucket
            msg = ("Bucket {0} does not exist for project {1}. "
                   "Creating bucket...").format(bucket_name,config.PROJECT_ID)
            print(msg)
            self._bucket = client.create_bucket(bucket_name)

        

    def _check_in_bucket(self, fname):
        """
        Check if a file is in the bucket. Raies IOError if not found.

        Arguments
        ---------
        fname : str
            Path to the file in the bucket

        """
        if not fname in self.files:
            msg = "File {} does not exist in bucket {}".format(fname, self.name)
            raise IOError(msg)

    @property
    def name(self):
        """
        Return the name of the bucket
        """
        return self._bucket.name


    @property
    def bucket(self):
        """
        Return the google.cloud.storage.Bucket object
        """
        return self._bucket
    

    
    def rename_file(self, fname, new_name):
        """
        Rename file in bucket
        
        Arguments
        ---------
        fname : str
            Old name
        new_name : str
            New name

        """
        self._check_in_bucket(fname)

        blob = self._bucket.blob(fname)
        self._bucket.rename_blob(blob, new_name)
        msg = "Rename file {} to {}".format(fname, new_name)
        

    @property
    def files(self):
        """
        List files in bucket
        """

        files=[]
        for b in self._bucket.list_blobs():
            files.append(b.name)

        return files

    def copy_file_to_bucket(self, src, bucket, dst=None):
        """
        Copy file in the current bucket to another bucket

        Arguments
        ---------
        src : str
            Path to the fiie in this bucket
        bucket : :class:`google.cloud.storage.Bucket`
            Destination bucket
        dst : str
            Path in the detination bucket. If not provided
            the same name as the source will be used

        """
        msg = "Expect bucket of type {}, got {}".format(google.cloud.storage.Bucket,
                                                        type(bucket))
        assert isinstance(bucket, google.cloud.storage.Bucket), msg

        self._check_in_bucket(src)

        if dst is None: dst = src
        
        self._bucket.copy_blob(src, bucket, dst)
        
                   
    def delete_file(self, fname):
        """
        Delete file in bucket

        Arguments
        ---------
        fname : str
            Path to file which should be deleted
        """
        if fname in self.files:
            self._bucket.delete_blob(fname)
            msg = "File {} deleted in bucket {}".format(fname,self.name)

        else:
            msg  = "File {} does not exists in bucket {}".format(fname,self.name)

        print(msg)

    def upload_file(self, src, dst=None, overwrite=True):
        """
        Upload file to bucket

        Arguments
        ---------
        src : str
            Path to the (local) file to be uploaded
        dst : str
            Path to the desination file (remote). If not provided
            the same name as the source will be used
        overwrite : bool
            It True, ovwerwrite file if it allready exists
t

        """
        if not os.path.isfile(src):
            msg=("File {} does not exist"
                 "Cannot upload file.").format(src)
            raise IOError(msg)

        if dst is None: dst = src
        
        blob = self._bucket.blob(dst)

        if blob.exists() and overwrite:
            msg = "Delete old file {}".format(dst)
            print(msg)
            blob.delete()

        blob.upload_from_filename(src)
        msg = "File {} uploaded to bucket {} at {}".format(src,self.name, dst)
        print(msg)
        


            

    def download_file(self, src, dst=None):
        """
        Download file from bucket
        
        Arguments
        ---------
        src : str
            Path to the (remote) file in the bucket
        dst : str
            Path to the destination file (local). If not provided
            the same name as the source will be used
        """

        self._check_in_bucket(src)

        if dst is None: dst = src

        blob = self._bucket.blob(src)
        blob.download_to_filename(dst)
        msg = "Download file {} from bucket {} to {}".format(src, dst, self.name)
        print(msg)
        
        

            

if __name__ == "__main__":
    buckets = list_buckets()
    bucket = Bucket(buckets[0])


    from IPython import embed; embed()
    exit()
    


    
